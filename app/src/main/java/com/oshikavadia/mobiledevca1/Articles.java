package com.oshikavadia.mobiledevca1;

/**
 * Created by Shane on 02/12/2017.
 */

public class Articles
{
    private int _id;
    private String articleName;

    public Articles(int _id, String articleName) {
        this._id = _id;
        this.articleName = articleName;
    }

    public Articles(String articleName) {
        this._id = 0;
        this.articleName = articleName;
    }

    public Articles() {

    }

    public int get_id() {
        return _id;
    }

    public String getArticleName() {
        return articleName;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    @Override
    public String toString() {
        return "Articles{" +
                "_id=" + _id +
                ", articleName='" + articleName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Articles articles = (Articles) o;

        if (_id != articles._id) return false;
        return articleName != null ? articleName.equals(articles.articleName) : articles.articleName == null;
    }

    @Override
    public int hashCode() {
        int result = _id;
        result = 31 * result + (articleName != null ? articleName.hashCode() : 0);
        return result;
    }
}

