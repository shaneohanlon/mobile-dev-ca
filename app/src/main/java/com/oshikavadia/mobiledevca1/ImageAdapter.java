package com.oshikavadia.mobiledevca1;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.oshikavadia.mobiledevca1.R;

import java.util.List;

public class ImageAdapter extends ArrayAdapter<MediaMetadata> {
    private Context mContext;
    List<MediaMetadata> items;

    public ImageAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<MediaMetadata> objects) {
        super(context, resource, textViewResourceId, objects);
        mContext = context;
        items = objects;
    }


    public int getCount() {
        return items.size();
    }

    public MediaMetadata getItem(int position) {
        return null;
    }


    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(250, 250));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageBitmap(items.get(position).getImage());
        return imageView;
    }

}