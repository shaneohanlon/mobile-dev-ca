package com.oshikavadia.mobiledevca1;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class PinnedArticles extends baseActivity {
    private DatabaseHelper db = new DatabaseHelper(this);
    private List<String> pinnedArticles = new ArrayList<>();
    private ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinned_articles);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        pinnedArticles = getPinnedArticles();
        ListView lv = (ListView) findViewById(R.id.search_history_list);
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, pinnedArticles);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            String title = pinnedArticles.get(position);
            Intent i = new Intent(this, article_content.class);
            i.putExtra("article", title);
            startActivity(i);
        });
    }

    public void clearPinnedArticles(View view) {
        db.deleteAllPinArticle();
        pinnedArticles.clear();
        pinnedArticles.addAll(getPinnedArticles());
        adapter.notifyDataSetChanged();
    }


    private List<String> getPinnedArticles() {
        List<Pinned> searches = db.getAllPinArticles();
        List<String> names = new ArrayList<>();
        for (Pinned pinnedArticles : searches) {

            names.add(pinnedArticles.getPinName());
        }
        return names;
    }
}
