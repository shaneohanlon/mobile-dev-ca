package com.oshikavadia.mobiledevca1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by Oshi on 07-Nov-17.
 */

public class ArticleAdapter extends ArrayAdapter<ArticleMetadata> {
    ImageView saveImg = null, pinImage = null;
    List<ArticleMetadata> items;
    Context mContext;

    public ArticleAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<ArticleMetadata> objects) {
        super(context, resource, textViewResourceId, objects);
        Log.i("ArticleAdapter", "inside");
        items = objects;
        mContext = context;
    }

    public int getCount() {
        return items.size();
    }

    public ArticleMetadata getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView,
                        ViewGroup parent) {
        Log.i("getView", "inside");
//        View row = super.getView(position, convertView, parent);
        if (convertView == null) {
            convertView = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.article_row_layout, null);
        }
        View row = convertView;
        ArticleViewHolder holder = (ArticleViewHolder) row.getTag();

        if (holder == null) {
            holder = new ArticleViewHolder(row);
            row.setTag(holder);
        }
        Log.d("holder", holder.toString());
        ArticleMetadata article = items.get(position);
        holder.name.setText(article.getName());
        holder.image.setImageBitmap(article.getImage());
        holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        saveImg = (ImageView) row.findViewById(R.id.save);
        saveImg.setTag(article);
        pinImage = (ImageView) row.findViewById(R.id.pin);
        pinImage.setTag(article);
        return (row);
    }
}
