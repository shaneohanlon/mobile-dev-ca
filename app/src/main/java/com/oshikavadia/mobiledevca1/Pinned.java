package com.oshikavadia.mobiledevca1;

/**
 * Created by Shane on 05/12/2017.
 */

public class Pinned
{
    private int _id;
    private String pinName;
    private String pinNormName;

    public Pinned(int _id, String pinName, String pinNormName) {
        this._id = _id;
        this.pinName = pinName;
        this.pinNormName = pinNormName;
    }

    public Pinned(String pinName) {
        this.pinName = pinName;
    }
    public Pinned()
    {
    }

    public Pinned(int _id, String pinName) {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getPinName() {
        return pinName;
    }

    public void setPinName(String pinName) {
        this.pinName = pinName;
    }

    public String getPinNormName() {
        return pinNormName;
    }

    public void setPinNormName(String pinNormName) {
        this.pinNormName = pinNormName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pinned that = (Pinned) o;

        if (_id != that._id) return false;
        if (pinName != null ? !pinName.equals(that.pinName) : that.pinName != null) return false;
        return pinNormName != null ? pinNormName.equals(that.pinNormName) : that.pinNormName == null;
    }

    @Override
    public int hashCode() {
        int result = _id;
        result = 31 * result + (pinName != null ? pinName.hashCode() : 0);
        result = 31 * result + (pinNormName != null ? pinNormName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Pinned{" +
                "_id=" + _id +
                ", pinName='" + pinName + '\'' +
                ", pinNormName='" + pinNormName + '\'' +
                '}';
    }
}
